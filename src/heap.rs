use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap};

#[derive(Serialize, Deserialize)]
pub struct Heap {
    pub header: Header,
    pub items: BTreeMap<String, Item>,
}

#[derive(Serialize, Deserialize)]
pub struct Header {
    pub memory_usage: i64,
    pub memory_usage_real: i64,
    pub peak_memory_usage: i64,
    pub peak_memory_usage_real: i64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Item {
    #[serde(rename = "type")]
    pub item_type: String,
    pub size: String,
    pub symbol_name: Option<String>,
    pub is_root: bool,
    pub frame: Option<String>,
    pub class: Option<String>,
    pub object_handle: Option<String>,
    pub children: Option<BTreeMap<String, String>>,
}
