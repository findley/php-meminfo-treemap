<?php
if (extension_loaded('meminfo')) {
    error_log('meminfo is enabled, heap dump will be capture during out-of-memory event');

    // Reserved memory so that heap dump can be created without triggering a secondary oom error
    $GLOBALS['oom_reserved_chunk'] = str_repeat('*', 1024 * 1024 * 10);

    function oom_heap_dump_shutdown() {
        $GLOBALS['reserve'] = null;
        extract($GLOBALS);

        $error = error_get_last();
        if($error !== null) {
            if (strpos($error['message'], 'Allowed memory size of') !== false) {
                error_log($error);
                $outputFile = '/tmp/php_heap_'.date('m-d-Y_hia').'.json';
                meminfo_dump(fopen($outputFile, 'w'));
            }
        }
    }
    register_shutdown_function("oom_heap_dump_shutdown");
}
