use std::collections::{HashSet, VecDeque};

use crate::tree::{TreeChildren, TreeSizes};

pub struct BfsIter<'b> {
    q: VecDeque<Option<&'b String>>,
    curr_depth: u64,
    max_depth: u64,
    max_children: u64,

    sizes: TreeSizes<'b>,
    all_children: TreeChildren<'b>,
}

impl<'b> BfsIter<'b> {
    pub fn new(
        root: &'b String,
        max_depth: u64,
        max_children: u64,
        all_children: TreeChildren<'b>,
        sizes: TreeSizes<'b>,
    ) -> BfsIter<'b> {
        let mut q = VecDeque::<Option<&'b String>>::new();
        q.push_back(Some(root));
        q.push_back(None);

        BfsIter {
            q: q,
            curr_depth: 0,
            max_depth: max_depth,
            max_children: max_children,

            all_children: all_children,
            sizes: sizes,
        }
    }
}

impl<'b> Iterator for BfsIter<'b> {
    type Item = (&'b String, Vec<&'b String>);

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(next_node) = self.q.pop_front() {
            match next_node {
                Some(node_id) => {
                    let mut children: Vec<&String> = self
                        .all_children
                        .get(node_id)
                        .unwrap_or(&HashSet::new())
                        .iter()
                        .cloned()
                        .collect();

                    children.sort_by(|a, b| {
                        let left = self.sizes.get(a).copied().unwrap_or(0);
                        let right = self.sizes.get(b).copied().unwrap_or(0);

                        right.cmp(&left)
                    });

                    let mut i = 0;
                    for child_id in children.iter() {
                        if i < self.max_children {
                            self.q.push_back(Some(child_id));
                        }
                        i += 1;
                    }

                    return Some((node_id, children));
                }
                None => {
                    self.curr_depth += 1;
                    if self.curr_depth >= self.max_depth || self.q.is_empty() {
                        break;
                    }
                    self.q.push_back(None);
                }
            }
        }

        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;

    #[test]
    fn test_single_node() {
        // Arrange
        let root = String::from("root");
        let children = HashMap::new();
        let sizes = HashMap::new();
        let iter = BfsIter::new(&root, std::u64::MAX, std::u64::MAX, children, sizes);

        // Act
        let result: Vec<(&String, Vec<&String>)> = iter.collect();

        // Assert
        assert_eq!(1, result.len());
        assert_eq!(root, *result[0].0);
        assert_eq!(0, result[0].1.len());
    }

    #[test]
    fn test_three_node() {
        // Arrange
        let root = String::from("root");
        let c1 = String::from("c1");
        let c2 = String::from("c2");

        let children = [(&root, [&c1, &c2].iter().cloned().collect())]
            .iter()
            .cloned()
            .collect();

        let mut sizes = TreeSizes::new();
        sizes.insert(&c1, 10);
        sizes.insert(&c2, 5);

        let iter = BfsIter::new(&root, std::u64::MAX, std::u64::MAX, children, sizes);

        // Act
        let result: Vec<(&String, Vec<&String>)> = iter.collect();

        // Assert
        assert_eq!(3, result.len());

        assert_eq!(root, *result[0].0);
        assert_eq!(c1, *result[1].0);
        assert_eq!(c2, *result[2].0);

        assert_eq!(2, result[0].1.len());
        assert_eq!(0, result[1].1.len());
        assert_eq!(0, result[2].1.len());
    }

    #[test]
    fn test_children_ordered_by_size() {
        // Arrange
        let root = String::from("root");
        let c1 = String::from("c1");
        let c2 = String::from("c2");
        let c3 = String::from("c3");

        let mut children = TreeChildren::new();
        children.insert(&root, [&c1, &c2, &c3].iter().cloned().collect());

        let mut sizes = TreeSizes::new();
        sizes.insert(&c1, 5);
        sizes.insert(&c2, 15);
        sizes.insert(&c3, 10);

        let iter = BfsIter::new(&root, std::u64::MAX, std::u64::MAX, children, sizes);

        // Act
        let result: Vec<(&String, Vec<&String>)> = iter.collect();

        // Assert
        assert_eq!(4, result.len());

        assert_eq!(root, *result[0].0);
        assert_eq!(c2, *result[1].0);
        assert_eq!(c3, *result[2].0);
        assert_eq!(c1, *result[3].0);

        assert_eq!(3, result[0].1.len());
        assert_eq!(0, result[1].1.len());
        assert_eq!(0, result[2].1.len());
        assert_eq!(0, result[3].1.len());
    }

    #[test]
    fn test_max_depth() {
        // Arrange
        let root = String::from("root");
        let c1 = String::from("c1");
        let c2 = String::from("c2");
        let c3 = String::from("c3");

        let mut children = TreeChildren::new();
        children.insert(&root, [&c1].iter().cloned().collect());
        children.insert(&c1, [&c2].iter().cloned().collect());
        children.insert(&c2, [&c3].iter().cloned().collect());

        let sizes = TreeSizes::new();

        let iter = BfsIter::new(&root, 3, std::u64::MAX, children, sizes);

        // Act
        let result: Vec<(&String, Vec<&String>)> = iter.collect();

        // Assert
        assert_eq!(3, result.len());

        assert_eq!(root, *result[0].0);
        assert_eq!(c1, *result[1].0);
        assert_eq!(c2, *result[2].0);
    }

    #[test]
    fn text_max_children() {
        // Arrange
        let root = String::from("root");
        let c1 = String::from("c1");
        let c2 = String::from("c2");
        let c3 = String::from("c3");

        let mut children = TreeChildren::new();
        children.insert(&root, [&c1, &c2, &c3].iter().cloned().collect());

        let mut sizes = TreeSizes::new();
        sizes.insert(&c1, 20);
        sizes.insert(&c2, 15);
        sizes.insert(&c3, 10);

        let iter = BfsIter::new(&root, std::u64::MAX, 2, children, sizes);

        // Act
        let result: Vec<(&String, Vec<&String>)> = iter.collect();

        // Assert
        assert_eq!(3, result.len());

        assert_eq!(root, *result[0].0);
        assert_eq!(c1, *result[1].0);
        assert_eq!(c2, *result[2].0);

        // All children are still on node, but not traversed
        assert_eq!(3, result[0].1.len());
    }
}
