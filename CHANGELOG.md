# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [1.0.2] - 2021-09-06
### Fixed
- Accomodate missing node IDs (due to extensions)

## [1.0.1] - 2021-08-31
### Added
- Update the label on "other" node to show the number of nodes

### Fixed
- Double quotes are now escaped in google chart json parent column

## [1.0.0] - 2021-08-27
### Added
- php-meminfo-treemap tool
- output types:
  - html (google chart treemap)
  - json (google chart data table format)
  - dot (graphviz dot file)
  - patchwork (graphviz dot file for patchwork)
- option `--depth` for specifying max depth to traverse
- option `--children` for specifying max children to traverse per node
- option `--output` for specifying an output file rather than stdout


[Unreleased]: https://gitlab.com/findley/php-meminfo-treemap/-/compare/master...v1.0.2
[1.0.2]: https://gitlab.com/findley/php-meminfo-treemap/-/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitlab.com/findley/php-meminfo-treemap/-/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/findley/php-meminfo-treemap/-/releases/v1.0.0
