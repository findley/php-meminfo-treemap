# php-meminfo-treemap Tasks

### Todo

- [ ] Allow custom html template for treemap
- [ ] Help user identify the root node they want to use
- [ ] Better colors for treemap

### In Progress

### Done ✓

- [x] Add release tag and gitlab release
  - [x] Figure out rust cross-compiling (add targets)
- [x] Readme changes
  - [x] Add instructions for installing and running (happy path)
  - [x] Add example for how php-meminfo can be used to generate a heap dump on oom error
  - [x] Add instructions to build from source / develop
- [x] Add a changelog
- [x] Add output type for google charts data table json
- [x] Add more tests
- [x] Cleanup/refactor crappy rust code
  - [~] Clearly separate Tree representation from meminfo dump representation
  - [x] Remove unused code
  - [x] Consolidate repeated code for tree display methods
- [x] Setup gitlab CI
- [x] Add some fancy command line arg parsing
  - [x] Add option for output type (dot, patchwork, google chart json)
  - [x] Add option for max depth
  - [~] Add option for min child size (% or fixed)
  - [x] Add option(s) for selecting the root node
  - [x] Write usage/manual
- [x] For google chart output, generate .html file directly (rather than json)
    - [x] Create template page
    - [x] Template json data into inline script on page
- [x] Initial shitcode dump
