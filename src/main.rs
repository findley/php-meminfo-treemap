use std::env;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::io::Write as _; // Imported for trait definitions
use std::path::Path;

use getopts::Options;

mod heap;
mod templates;
mod tree;
mod tree_bfs;

use heap::Heap;
use tree::Tree;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optopt("o", "output", "output file name (default stdout)", "NAME");
    opts.optopt(
        "t",
        "type",
        "output type (html, dot, patchwork, json) (default html)",
        "TYPE",
    );
    opts.optopt(
        "d",
        "depth",
        "Maximum depth to display (default 5)",
        "DEPTH",
    );
    opts.optopt(
        "c",
        "children",
        "Maximum children to display per node (default 30)",
        "CHILDREN",
    );
    opts.optflag("h", "help", "print this help menu");

    let matches = opts.parse(&args[1..])?;

    if matches.opt_present("h") || matches.free.len() != 2 {
        print_usage(&program, opts);
        return Ok(());
    }

    let output_file = matches.opt_str("o");
    let output_type =
        OutputType::from_string(&matches.opt_str("t").unwrap_or("html".to_string()));
    let max_depth = matches
        .opt_str("d")
        .map(|d| d.parse::<u64>().ok())
        .flatten()
        .unwrap_or(5);
    let max_children = matches
        .opt_str("c")
        .map(|c| c.parse::<u64>().ok())
        .flatten()
        .unwrap_or(30);

    let input = matches.free[0].clone();
    let root = matches.free[1].clone();

    let heap = load_heap(input)?;
    let tree = Tree::from_heap(&heap, &root)?;

    let output = match output_type {
        OutputType::Html => tree.to_gchart_html(max_depth, max_children, &matches.free[0])?,
        OutputType::Dot => tree.to_dot(max_depth, max_children)?,
        OutputType::Patchwork => tree.to_patch(max_depth, max_children)?,
        OutputType::GChartJson => tree.to_gchart_json(max_depth, max_children)?,
        _ => panic!("Unknown output type"),
    };

    match output_file {
        Some(filename) => {
            let mut file = File::create(filename)?;
            file.write_all(output.as_bytes())?;
        }
        None => {
            println!("{}", output);
        }
    }

    Ok(())
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} FILE ROOT [options]", program);
    print!("{}", opts.usage(&brief));
}

fn load_heap<P: AsRef<Path>>(path: P) -> Result<Heap, Box<dyn Error>> {
    let file = File::open(path)?;
    let reader = BufReader::new(file);

    let heap: Heap = serde_json::from_reader(reader)?;

    Ok(heap)
}

enum OutputType {
    Html,
    Dot,
    Patchwork,
    GChartJson,
    Unknown,
}

impl OutputType {
    fn from_string(str: &String) -> Self {
        match str.as_str() {
            "html" => OutputType::Html,
            "dot" => OutputType::Dot,
            "patchwork" => OutputType::Patchwork,
            "json" => OutputType::GChartJson,
            _ => OutputType::Unknown,
        }
    }
}

#[cfg(test)]
mod tests {}
