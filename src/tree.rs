use std::collections::{BTreeMap, HashMap, HashSet, VecDeque};
use std::error::Error;
use std::fmt::Write as _; // Imported for trait definitions

use crate::heap::{Heap, Item};
use crate::templates;
use crate::tree_bfs::BfsIter;

pub type TreeSizes<'a> = HashMap<&'a String, u64>;
pub type TreeChildren<'a> = HashMap<&'a String, HashSet<&'a String>>;

pub struct Tree<'a> {
    root: &'a String,
    all_children: TreeChildren<'a>,
    sizes: TreeSizes<'a>,
    names: HashMap<&'a String, &'a String>,
    items: &'a BTreeMap<String, Item>,
}

impl<'a> Tree<'a> {
    pub fn from_heap<'b>(
        heap: &'b Heap,
        root: &'b String,
    ) -> Result<Tree<'b>, Box<dyn Error>> {
        let mut all_children = TreeChildren::new();
        let mut names = HashMap::<&String, &String>::new();

        let mut queue = VecDeque::<&String>::new();
        let mut visited = HashSet::<&String>::new();

        visited.insert(root);
        queue.push_back(root);

        while let Some(node_id) = queue.pop_front() {
            let item = heap
                .items
                .get(node_id);

            if item.is_none() {
                continue;
            }

            let unvisited_children: Vec<(&String, &String)> = item.unwrap()
                .children
                .iter()
                .flatten()
                .filter(|(_, child_id)| !visited.contains(child_id))
                .collect();

            for (name, child_id) in unvisited_children.iter() {
                visited.insert(child_id);
                queue.push_back(child_id);
                names.insert(child_id, name);
            }

            let child_ids = unvisited_children
                .iter()
                .map(|(_, child_id)| *child_id)
                .collect();
            all_children.insert(node_id, child_ids);
        }

        let mut tree = Tree {
            root: root,
            all_children: all_children,
            sizes: TreeSizes::new(),
            names: names,
            items: &heap.items,
        };

        tree.calc_size()?;

        Ok(tree)
    }

    pub fn to_gchart_html(
        &'a self,
        max_depth: u64,
        max_children: u64,
        input_filename: &String,
    ) -> Result<String, Box<dyn Error>> {
        let json_table = self.to_gchart_json(max_depth, max_children)?;

        Ok(templates::google_chart_treemap(&json_table, input_filename))
    }

    pub fn to_gchart_json(
        &'a self,
        max_depth: u64,
        max_children: u64,
    ) -> Result<String, Box<dyn Error>> {
        let mut json_table = String::new();

        let default_root_name = &String::from("root");
        let root_size = self.get_size(self.root);
        let root_id = treemap_id(&String::from("root"), self.root, root_size);

        write!(json_table, "[")?;
        writeln!(json_table, r#"["Node", "Parent", "Bytes", "Type"],"#)?;
        write!(json_table, "[\"{}\", null, {}, 6]", root_id, root_size)?;

        for (node_id, children) in self.iter_bfs(max_depth, max_children) {
            let parent_name = self
                .names
                .get(node_id)
                .unwrap_or(&default_root_name)
                .replace("\"", "");
            let parent_size = self.get_size(node_id);
            let parent = treemap_id(&parent_name, node_id, parent_size);

            let mut other_size = 0;
            let mut other_count = 0;
            let mut i = 0;
            for child_id in children.iter() {
                let size = self.get_size(child_id);

                if i < max_children {
                    let item_type: &String = &self.items.get(child_id.clone()).unwrap().item_type;
                    let name = self.names.get(child_id).unwrap().replace("\"", "");
                    let color = type_to_color(item_type);
                    let child = treemap_id(&name, child_id, size);
                    writeln!(json_table, ",")?;
                    write!(
                        json_table,
                        r#"["{}", "{}", {}, {}]"#,
                        child, parent, size, color
                    )?;
                } else {
                    other_size += size;
                    other_count += 1;
                }
                i += 1;
            }

            if other_size > 0 {
                let other = treemap_id(&format!("Other ({} nodes)", other_count), node_id, other_size);
                writeln!(json_table, ",")?;
                write!(
                    json_table,
                    r#"["{}", "{}", {}, 3]"#,
                    other, parent, other_size
                )?;
            }
        }

        write!(json_table, "]")?;

        Ok(json_table)
    }

    pub fn to_patch(
        &'a self,
        max_depth: u64,
        max_children: u64,
    ) -> Result<String, Box<dyn Error>> {
        let mut result = String::new();
        writeln!(result, "digraph {{")?;
        let root_size = self.get_size(self.root);

        for (node_id, children) in self.iter_bfs(max_depth, max_children) {
            let mut other_size = 0;
            let mut i = 0;
            for child_id in children.iter() {
                let size = self.get_size(child_id);

                if i < max_children {
                    let perc = (size as f64) / (root_size as f64);
                    let label_size = (perc * 30000.0).log(1.5) + 1.0;
                    let name = self.names.get(child_id).unwrap();
                    writeln!(
                        result,
                        "\"{}\" [area={}, label=\"{}:{}\", fontsize=\"{}\", tooltip=\"{}\"]",
                        child_id, perc * 1000.0, name, display_size(size), label_size, name
                    )?;
                } else {
                    other_size += size;
                }
                i += 1;
            }

            if other_size > 0 {
                let perc = (other_size as f64) / (root_size as f64);
                let label_size = (perc * 30000.0).log(1.5) + 1.0;
                writeln!(
                    result,
                    "\"{}\" [area={}, label=\"other:{}\", fontsize=\"{}\"]",
                    format!("{}_extra", node_id),
                    perc * 1000.0,
                    display_size(other_size),
                    label_size
                )?;
            }
        }

        writeln!(result, "}}")?;
        Ok(result)
    }

    pub fn to_dot(
        &'a self,
        max_depth: u64,
        max_children: u64,
    ) -> Result<String, Box<dyn Error>> {
        let mut result = String::new();

        writeln!(result, "digraph {{")?;

        let root_size = self.get_size(self.root);
        writeln!(
            result,
            "\"{}\" [area={}, label=\"root:{}\"]",
            self.root, root_size, root_size
        )?;

        for (node_id, children) in self.iter_bfs(max_depth, max_children) {
            let mut list = String::new();
            let mut first = true;
            for child_id in children.iter() {
                if first {
                    first = false;
                    write!(list, "\"{}\"", child_id)?;
                } else {
                    write!(list, ", \"{}\"", child_id)?;
                }

                let size = self.get_size(child_id);
                let name = self.names.get(child_id).unwrap();
                writeln!(
                    result,
                    "\"{}\" [label=\"{}:{}\"]",
                    child_id,
                    name,
                    display_size(size)
                )?;
            }

            if !list.is_empty() {
                writeln!(result, "\"{}\" -> {{ {} }}", node_id, list)?;
            }
        }

        writeln!(result, "}}")?;
        Ok(result)
    }

    pub fn get_size(&self, id: &String) -> u64 {
        *self.sizes.get(id).unwrap_or(&0)
    }

    fn calc_size(&mut self) -> Result<(), Box<dyn Error>> {
        let mut stack = Vec::<&String>::new();

        let iter = BfsIter::new(
            self.root,
            std::u64::MAX,
            std::u64::MAX,
            self.all_children.clone(),
            self.sizes.clone(),
        );

        for (node_id, children) in iter {
            if children.is_empty() {
                let size = self
                    .items
                    .get(node_id)
                    .map(|n| n.size.parse::<u64>().unwrap())
                    .unwrap_or(0);
                self.sizes.insert(node_id, size);
            } else {
                stack.push(node_id);
            }
        }

        while let Some(node_id) = stack.pop() {
            let tree_size: u64 = self
                .all_children
                .get(node_id)
                .map(|c| {
                    c.iter().try_fold(0, |acc, child_id| {
                        self.sizes.get(child_id).map(|child_size| *child_size + acc)
                    })
                })
                .flatten()
                .ok_or(format!("Children missing sizes at node {}", node_id))?;

            let self_size = self
                .items
                .get(node_id)
                .map(|n| n.size.parse::<u64>().unwrap())
                .unwrap_or(0);
            self.sizes.insert(node_id, self_size + tree_size);
        }

        Ok(())
    }

    fn iter_bfs(
        &self,
        max_depth: u64,
        max_children: u64,
    ) -> BfsIter {
        BfsIter::new(
            self.root,
            max_depth,
            max_children,
            self.all_children.clone(), // @@@ Anyway to avoid cloning this?
            self.sizes.clone(),
        )
    }
}

fn display_size(size: u64) -> String {
    match size {
        s if s < 1000 => format!("{}B", size),
        s if s < 1_000_000 => format!("{:.1}KB", size as f64 / 1000.0),
        _ => format!("{:.1}MB", size as f64 / 1_000_000.0),
    }
}

fn treemap_id(name: &String, id: &String, size: u64) -> String {
    format!("{} {} ({})", name, display_size(size), id)
}

fn type_to_color(item_type: &String) -> i64 {
    match item_type.as_str() {
        "array" => 1,
        "string" => 2,
        "float" => 3,
        "int" => 4,
        "bool" => 5,
        "object" => 6,
        "null" => 6,
        "resource" => 6,
        _ => 6,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn simple_with_cycles() {
        let data = r#"
            {
              "header" : {
                "memory_usage" : 139174112,
                "memory_usage_real" : 159387648,
                "peak_memory_usage" : 145966552,
                "peak_memory_usage_real" : 161484800
              },
              "items": {
                "a" : {
                    "type" : "object",
                    "size" : "5",
                    "is_root" : false,
                    "children" : {
                        "b":"b",
                        "c":"c"
                    }
                },
                "b" : {
                    "type" : "object",
                    "size" : "5",
                    "is_root" : false,
                    "children" : {
                        "d":"d"
                    }
                },
                "c" : {
                    "type" : "object",
                    "size" : "5",
                    "is_root" : false,
                    "children" : {
                        "d":"d",
                        "g":"g"
                    }
                },
                "d" : {
                    "type" : "object",
                    "size" : "5",
                    "is_root" : false,
                    "children" : {
                        "e":"e",
                        "f":"f"
                    }
                },
                "e" : {
                    "type" : "object",
                    "size" : "5",
                    "is_root" : false,
                    "children" : {
                        "b":"b"
                    }
                },
                "f" : {
                    "type" : "object",
                    "size" : "5",
                    "is_root" : false,
                    "children" : {
                        "c":"c"
                    }
                },
                "g" : {
                    "type" : "object",
                    "size" : "5",
                    "is_root" : false
                }
              }
            }
        "#;

        let heap: Heap = serde_json::from_str(data).unwrap();
        let root = String::from("a");
        let tree = Tree::from_heap(&heap, &root).unwrap();

        assert_node_size("g", 5, tree.get_size(&String::from("g")));
        assert_node_size("f", 5, tree.get_size(&String::from("e")));
        assert_node_size("e", 5, tree.get_size(&String::from("f")));
        assert_node_size("d", 15, tree.get_size(&String::from("d")));
        assert_node_size("b", 20, tree.get_size(&String::from("b")));
        assert_node_size("c", 10, tree.get_size(&String::from("c")));
        assert_node_size("a", 35, tree.get_size(&String::from("a")));
    }

    fn assert_node_size(node: &str, expected: u64, actual: u64) {
        assert_eq!(
            expected, actual,
            "Expected node {} to be {}, got {}",
            node, expected, actual
        );
    }
}
